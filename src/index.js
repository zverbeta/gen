import './scss/index.scss';

import {Slider} from './slider';

window.addEventListener('load', function(){
  const slider1 = new Slider('slider-1', {
    showSlides: 1
  });
  
  
  const slider2 = new Slider('slider-2', {
    showSlides: 'auto',
    slideGap: 50
  });  
});