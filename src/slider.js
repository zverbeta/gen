import './scss/slider.scss';

export function Slider(selector, settings) {
  this.selector = selector;
  this.currentIndex = 0;
  this.settings = Object.assign({
    showSlides: 1,
    slideGap: 0,
    sliderListSelector: '.slider-list',
    slierListItemSelector: '.slider-list__item',
    sliderWrapperSelector: '.slider-wrapper',
    arrowLeftSelector: '.slider-arrows__left',
    arrowRightSelector: '.slider-arrows__right'
  }, settings);

  this.init();
}

Slider.prototype.init = function() {
  this.sliderContainer = document.getElementById(this.selector);
  this.sliderList = this.sliderContainer.querySelector(this.settings.sliderListSelector);
  this.sliderWrapper = this.sliderContainer.querySelector(this.settings.sliderWrapperSelector);
  this.slides = this.sliderContainer.querySelectorAll(this.settings.slierListItemSelector);
  this.leftArrow = this.sliderContainer.querySelector(this.settings.arrowLeftSelector);
  this.rightArrow = this.sliderContainer.querySelector(this.settings.arrowRightSelector);
  this.slidesAsArray = Array.from(this.slides);
  this.totalSlides = this.slides.length;

  const slidesWidth = (this.sliderWrapper.clientWidth / this.settings.showSlides);
  const calcSlidesWidth = this.slidesAsArray.reduce((acc, el) => acc += el.clientWidth + this.settings.slideGap, 0);
  let allSlidesWidth = 0;
  
  if (this.settings.showSlides == 1) {
    this.sliderWrapper.style.width = `${calcSlidesWidth / this.totalSlides}px`
  }

  this.slidesAsArray.forEach((slide, index) => {
    slide.style.marginRight = `${this.settings.slideGap}px`;
    slide.style.width = `${slidesWidth}px`;
  })

  if (this.settings.showSlides == 'auto') {
    allSlidesWidth = `${calcSlidesWidth}px`;
  } else {
    allSlidesWidth = `${slidesWidth * this.totalSlides}px`;
  } 

  this.sliderList.style.width = allSlidesWidth;

  this.arrowHandlers();
}

Slider.prototype.arrowHandlers = function() {
  this.leftArrow.addEventListener('click', (e) => {
    e.preventDefault();
    this.slideToLeft();
  })

  this.rightArrow.addEventListener('click', (e) => {
    e.preventDefault();
    this.slideToRight();
  })
}

Slider.prototype.slideToLeft = function() {
  this.currentIndex--;

  if (this.currentIndex == 0) {
    this.currentIndex = 0;
  }
  
  // Turn on loop for first slide 
  if (this.currentIndex < 0) {
    this.currentIndex = this.totalSlides - 1;
  }

  this.slideTo(this.currentIndex);
}

Slider.prototype.slideToRight = function() {
  this.currentIndex++;

  if (this.currentIndex == this.totalSlides - 1) {
    this.currentIndex = this.totalSlides - 1;
  }

  // Turn on loop for last slide
  if (this.currentIndex > this.totalSlides - 1) {
    this.currentIndex = 0;
  }

  this.slideTo(this.currentIndex);
}

Slider.prototype.slideTo = function(index) {
  const currentSlide = this.slides[index];

  this.sliderList.style.left = `-${currentSlide.offsetLeft}px`; 
}

